/*
 * MYI2C.cpp
 *
 *  Created on: 24. 11. 2018
 *      Author: pc
 */

#include <MYI2C.h>

MY_I2C::MY_I2C(I2C_Type* I2C_base, uint32_t frequency_i2c) {
	this->I2C_base = I2C_base;
	I2C_MasterGetDefaultConfig(&master_config);
	I2C_MasterInit(I2C_base, &master_config, frequency_i2c);

	memset(&master_xfer, 0, sizeof(master_xfer));
}

MY_I2C::~MY_I2C() {
	// TODO Auto-generated destructor stub
}

void MY_I2C::i2c_read(uint8_t s_addr, uint8_t reg_addr, uint8_t* data, uint8_t len) {
	master_xfer.slaveAddress=s_addr;
	master_xfer.direction = kI2C_Read;
	master_xfer.subaddress = reg_addr;
	master_xfer.subaddressSize = 1;
	master_xfer.data = data;
	master_xfer.dataSize = len;
	master_xfer.flags = kI2C_TransferDefaultFlag;
	I2C_MasterTransferBlocking(I2C_base, &master_xfer);
}
void MY_I2C::i2c_write(uint8_t s_addr, uint8_t reg_addr, uint8_t* data, uint8_t len) {
	master_xfer.slaveAddress=s_addr;
	master_xfer.direction = kI2C_Write;
	master_xfer.subaddress = reg_addr;
	master_xfer.subaddressSize = 1;
	master_xfer.data = data;
	master_xfer.dataSize = len; //+1
	master_xfer.flags = kI2C_TransferDefaultFlag;
	I2C_MasterTransferBlocking(I2C_base, &master_xfer);
}

