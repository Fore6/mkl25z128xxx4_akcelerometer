/* Copyright (c) 2010-2011 mbed.org, MIT License
*
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software
* and associated documentation files (the "Software"), to deal in the Software without
* restriction, including without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all copies or
* substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
* BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
* DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "MMA8451Q.h"
#include <math.h>

#define REG_WHO_AM_I      0x0D
#define REG_CTRL_REG_1    0x2A
#define REG_OUT_X_MSB     0x01
#define REG_OUT_Y_MSB     0x03
#define REG_OUT_Z_MSB     0x05

#define PI 3.141592653589793

#define UINT14_MAX        16383

MMA8451Q::MMA8451Q(uint8_t i2c_addr){
	this->i2c_addr = i2c_addr;
	i2c = new MY_I2C(I2C0, CLOCK_GetBusClkFreq());

    uint8_t data[1] = {0x01};
    writeRegs(REG_CTRL_REG_1, data, 1);
}

MMA8451Q::~MMA8451Q() {
	delete i2c;
}

uint8_t MMA8451Q::getWhoAmI() {
    uint8_t who_am_i = 0;
    readRegs(REG_WHO_AM_I, &who_am_i, 1);
    return who_am_i;
}

float MMA8451Q::getAccX() {
    return (float(getAccAxis(REG_OUT_X_MSB))/4096.0);
}

float MMA8451Q::getAccY() {
    return (float(getAccAxis(REG_OUT_Y_MSB))/4096.0);
}

float MMA8451Q::getAccZ() {
    return (float(getAccAxis(REG_OUT_Z_MSB))/4096.0);
}

void MMA8451Q::getAccAllAxis(float * res) {
    res[0] = getAccX();
    res[1] = getAccY();
    res[2] = getAccZ();
}

void MMA8451Q::high_pass_filter(float* data, float* current, float* previous, int dT_us, int fc_Hz) {
	float alfa = (float)1 / (2*PI * ((float)dT_us/1000000) * (float)fc_Hz + (float)1 );
	//high-pass filter for X
	data[0]= alfa*data[0] + (alfa * (current[0]-previous[0]) );
	//high-pass filter for Y
	data[1]= alfa*data[1] + (alfa * (current[1]-previous[1]) );
	//high-pass filter for Z
	data[2]= alfa*data[2] + (alfa * (current[2]-previous[2]) );

	/**
	 * posum vzoriek vzorka(i-1) = vzorka(i)
	 * v pripade nutnosti zachovania vzoriek odstrániť!
	 */

	previous[0]=current[0];
	previous[1]=current[1];
	previous[2]=current[2];

}

void MMA8451Q::down_pass_filter(float* data, float* current, int dT_us, float fc_Hz) {
	float alfa = ( 2*PI * ((float)dT_us/1000000) * (float)fc_Hz ) / ( 2*PI * ((float)dT_us/1000000) * (float)fc_Hz + (float)1 );

	//down-pass filter for X
	data[0]= alfa*current[0] + (1-alfa)*data[0];
	//down-pass filter for Y
	data[1]= alfa*current[1] + (1-alfa)*data[1];
	//down-pass filter for Z
	data[2]= alfa*current[2] + (1-alfa)*data[2];
}

float MMA8451Q::getNaklonX(float* low_pass_data){
	return (360/(2*PI)) * atan((low_pass_data[0])/(sqrt( pow( low_pass_data[1],2) + pow(low_pass_data[2],2) )));
}

float MMA8451Q::getNaklonY(float* low_pass_data){
	return (360/(2*PI)) * atan((low_pass_data[1])/(sqrt( pow( low_pass_data[0],2) + pow(low_pass_data[2],2) )));
}

int16_t MMA8451Q::getAccAxis(uint8_t addr) {
    int16_t acc;
    uint8_t res[2];
    readRegs(addr, res, 2);

    acc = (res[0] << 6) | (res[1] >> 2);
    if (acc > UINT14_MAX/2)
        acc -= UINT14_MAX;

    return acc;
}

void MMA8451Q::readRegs(uint8_t reg_addr, uint8_t * data, int len) {
	i2c->i2c_read(i2c_addr, reg_addr, data, len);
}

void MMA8451Q::writeRegs(uint8_t reg_addr, uint8_t * data, int len) {
	i2c->i2c_write(i2c_addr, reg_addr, data, len);
}
