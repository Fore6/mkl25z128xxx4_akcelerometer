/*
 * MYI2C.h
 *
 *  Created on: 24. 11. 2018
 *      Author: pc
 */

#ifndef MYI2C_H_
#define MYI2C_H_

#include <fsl_i2c.h>

class MY_I2C {
private:
	I2C_Type* I2C_base;
	i2c_master_config_t master_config;
	i2c_master_transfer_t master_xfer;
public:
	MY_I2C(I2C_Type* I2C_base, uint32_t frequency_i2c);
	virtual ~MY_I2C();

	void i2c_read(uint8_t s_addr, uint8_t reg_addr, uint8_t* data, uint8_t len);
	void i2c_write(uint8_t s_addr, uint8_t reg_addr, uint8_t* data, uint8_t len);
};

#endif /* MYI2C_H_ */
